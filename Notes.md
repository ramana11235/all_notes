
# Tasks-To-Do
1. Finish the commonly used functions exercise
2. Start/Complete the wildcards exercise
3. You need to add how to run scripts on servers 
4. Combine README_bash_and_md with README_linux_cheat_sheet with README_bash_env_variables
---
# Temp Notes To Sort 


--- 
### Security and ports
- A reverse proxy setup where server access to the world (port 80) has been cut off, and another server (the proxy server) has been given inbound connection with port 80. The proxy server (which IS the reverse proxy) then routes the inbound traffic through another port (e.g. 8080) to our server. The reverse proxy and our server have separate ip addresses 
  - check filipes diagram on trello
- A security group is just a list of ips and ports that allow access into the server (same thing as firewall)
- A subnet is a collection of servers (which all each have their own sg(security group)) which are all housed under something
  - So if a subnet has an ip of 16.20.0.0/16
    - The servers/devices within the subnet has ips of 16.20.1.0/24 and 16.20.2.0/24 and 16.20.3.0/24
    - (We'll cover the /16 and /24 later, we better fam)
  - The subnet itself is protected by NACL (network access control lists) which has a lists of IP's which allow ip's in AND out.
    - Apparently the out is important for NACL 
---



## Script outline for installing paint js 

- `$PATH` is a path to all the shortcuts that's been set
  - all the "paths" are set to the variable and are separated by `:` between them
  - This is result of `echo $PATH`, all the shortcuts are set in one long variable.
```
/opt/homebrew/bin:/opt/homebrew/sbin:/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Applications/Visual Studio Code.app/Contents/Resources/app/bin
  ```
  - so to set a new path `PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH`
    - Via interpration rules, all the `$` stuff are expanded out before the new `$PATH` variable is set with the 


- for js things
  - for example `npm` (node package manager) needs a json file with the dependances and maybe other stuff to know what to install 
 
 
- `sudo sh -c "echo ..."` runs it as special permission script?
  - `sh` is a different type of sheel (the most basic) and `-c` is the command option to run shell as non interactive

- `<<_END_` two greater than to let shell expect a message till the secon `_END_`
- `>` one greater than means shell will expect a file to follow it 
  - `grep root </etc/passwd` = `cat /etc/passwd | grep root`
    - The output is exactly the same
    - 
- can use `cat <file> <<_EOF_` and end marker and `echo` to put a message in a file, cat is better because quotation formats is less strict because it waits for the end marker (_EOF_) to end the message

- `chkconfig 2345 20 80` 
  - `chkconfig` checks the config 
  - `2345` these are the levels the file is being run at 
    - This file is being run at levels `2`, `3`, `4`, `5`
    - Each of these levels have a meaning, e.g. `level 3` means Multi-User Mode with Networking
  - `20 80` is the "operation number" (i've made this term up)
    - It will tell the script to run as the 20th thing and stop as the 80th thing
    - e.g. `99 99` would mean the last thing to start and stop

- `kill -9 $(ps -ef | grep npm | awk '{print $2})` 
  - from a list of processes (`ps -ef`), find a process npm (`grep npm`), and choose only the second column (`awk '{print $2}'`)
  - if we do `ps -ef` this will return the PID (process ID) of any process with an npm in it
  - this is sent to kill -9 which kills process
  - `egrep` is extendended grep 
    - `kill -9 $(ps -ef | egrep 'npm|node' | awk '{print $2})`
    - the single quotes around npm|node means egrep looks for npm OR node OR both 
- `nohup npm run dev &`
  - `nohup` means it will run persistently until as much is done
  - `&` meeans it will run in the background


- `sed` is non interactive stream editor
  - `sed '' /etc/init.d/jspaint`
    - prints the file on the screen like normal
  - `sed 's,nohup npm,nohup /usr/bin/npm,' /etc/init.d/jspaint`
    - `s` searches string to substitue
    - `,` comma is a separator between text
    - the sytanx for `sed` is like 
      - `sed 's, <string to search>,<string to replace with>,' <path to file>`
      - you can add further options after the last word that is being replaced with 
        - `sed 's/root/ROOT/g' /etc/passwd/`
        - the `g` option makes it global and searches for all instances within the line 
        - and ANY character after `s` is used as the separator for the character
  - `sed -i` puts the change IN place the file 


- a service is a program that runs in the background


- For the line below we need `sudo sh -c` because without the `sh -c`, the redirection happens first as normal user and then the file `jspaint.conf` waits for a `sudo echo` which a normal user wouldnt have access to. So using `sh -c "...."` the whole command is a `sudo` command for the `sh` shell, no ambiguity of user privelage. 
```
sudo sh -c "echo \"<VirtualHost *:80>
  ProxyPreserveHost On

  ProxyPass / http://127.0.0.1:8080/
  ProxyPassReverse / http://127.0.0.1:8080/

</VirtualHost>\" >jspaint.conf"
```

- `kill -9` 
  - The `kill` without an option is just a terminate signal and doesn't clean up the mess after death
  - `-9` option cleans up all the mess after a murder 

## creating a user
- `sudo useradd -m ramana`
  - `useradd` adds a user called ramana
  - `-m` creates user file in /home for user 
    - also a file added in `/etc/passwd` for user
  - is locked as command doesn't add a passwd, we give it one with `sudo passwd ramana`
- `su - ramana` can switch user with password
  - `-` says i want to be the user fully, inherit full usage with paths, and other stuff that is user specific 
  - we also move to users home directory `/home/ramana`
  - you can check with command `id`
  - if u did `su ramana`, the id changes to `ramana` but you don't get moved to `/etc/ramana`
  - each user has their own `ssh key`, the private key always connects to you to a `user` not the server 
- `cd /etc/ssh/` 
  - accessing ssh file to change `sshd_config` (ssh deimon)
    - `ssh_config` is more like the client 
    - `sshd_config` is about the logging on 
  - This file has passwordauthentication set to no, change it to yes to make allow it access by password
  - To reload this process you need `sudo systemctl reload sshd` because this is a separate process
    - I thought it was `sourcez command but that for refresing environment variables and things for interactive shell stuff?
  - But this user isn't a `sudoer`, cant `sudo`, lets change that
    - `sudo ls /etc/sudoers.d` to access directory of sudo users
      - there are other directories for other sudo groups and dat idk
    - within that we create a file which we edit to add permissions and users
      - `sudo nano /etc/sudoers.d/91-ramana`
        - we write `ramana ALL=(ALL) ALL`
        - First COLUMN IS USER WE'RE ALLOWING TO ELECATE PRIVELAGE
        - `ALL=(ALL)` - usually before `=` there could is ip to restrict by connecting hosts
          - in the `()` is the user you wish to become so you can sudo as ANY user `(ALL)`
        - Third Column `NOPASSWD:ALL` means doesn't need to specify passwd
          - but we write just `ALL` to allow ALL commands \


## Kernel tuning
- theres lots of files in `/proc` (procedures)
  - and within that in `sys` there aare also more 
  - to change ipv4 port forwarding we do (an example of kernel tuning)
    - `cd proc/`
    - `cd sys/net/ipv4/`
    - if we `cat ip_forward` we get an output of 0
    - we can then `sudo sh -c 'echo "1" >ip_forward'` to change value to one
    - and check with `sudo sysctl net.ipv4.ip_forward`
    - we can even change back with `sudo sysctl net.ipv4.ip_forward=0`
- `sudo ls /etc/sysctl.d`  `sysctl.d` this is the equivalent of `.bash_profile` to ensure the changes remain after logging in and out
  - stores kernel settings
- `sudo sysctl -w net.ipv4.ip_forward=1` the `-w` option sets the value AND stores the value, should make a change and write it to a config file in 
  - This file is in `/proc` which is also in memory and so sysctl changes are made in memory
  - the way `sysctl` works is by 
- `sudo init 6` is a reboot of machine (dont normally do for production machines)
- as sudo `sysctl -w command..` didnt work (for whatever reason), we create a new `.conf` file (called `01-network.conf`) and write `net.ipv4.ip_forward=1`
  - we can then set the new file by `sysctl -p /etc/sysctl.d/01-network.conf`
  - and check to make sure by `sysctl net.ipv4.ip_forward`


- `lsmod`, `insmod` are all commands to add features and device drivers to the kernel which is external to it
  - First need to add `mke` to compile to code of the driver before `insmod`
  - and `rmmod` to remove any driver from the kernel
  - `sysctl` is changing the settings of the kernel with stuff already within the kernel 


## Drivers
- device drives are code which have file extension `.ko` 
- `/dev/` is like the door which allows the talking with the device driver and the keyboard
- ssd is solid because its like ram but it has persistance (whereby it doesn't dissapear)
  - They are battery backed up so they keep information even after they turn off 
- `sudo fdisk -l` to check disks 
- To partition a disk xvdf (This needs to be done in order to use this disk in the first place, even if we partition into 1, it needs to be done for kernel to talk to thing or some shit )
  - `sudo fdisk /dev/xvdf`
  - after 4 primary partitions, only extended logical partitions are allowed
  - and then just go through the options a
  - `sudo partprobe -s` to check if the partition has been recognised 
  - this is just initialising a disk making it ready for use, with the partition table keep track of how the disk is partitioned
  - different filesystems can be created in different partitions 
  - and all 3 partitions need to be mounted in different directories
- To make a filesystem???? (wtf is a filesystem)
  - `sudo mkfs -t ext3 /dev/xvdf1` 
    - `xvdf1` is the partition we want to format  
    - Lots of different file systems to chose from like ntfs and all that crap, google what a file system is 
    - command also shows number of inodes with `ls -i` and inode is a unique file with identifying number
- but if we `df -h` we only see one disk, not our partition because it's not mounted, to do dis:
    - `sudo mkdir /spare`
    - `sudo mount /dev/xvdf1 /spare` this is temporary and the mount connects the formatted disk with our tree structure through directory called `spare`
    - Whatever we write into `spare` we write into the new disk
    - `ls -a` in `/spare` shows a `lost+found` which means your at a "mount" place
- but if we unmount with `sudo umount /spare` 
  - and then `ls /spare` 
    - there won't be anything in the folder because the data is on the new disk and not on the directory
    - so `/spare` is a directory on the initial disk 
    - we can mount in the disk in any directory
      - so remounting the new disk in a new directory would be mean any created files would now be in that new directory and `/spare` would be empty 

- any files in the disk is unique to to the disk regardless of where it is mounted
  - if the disk getse mounted on a directory where files already existed in that directory, the files in the disk is what would be shown, the files on the directory get hidden until the disk is unmounted 

- `sudo blkid`
  - find the UUID for the disk u want to automatically mount
- `sudo nano /etc/fstab`
  - this is the file read for automatic mount
  - `UUID=55b5e483-bf33-461c-bfe9-66ecd058d112     /spare	  ext3   defaults 1 2`
  - UUID is the id of the disk, /spare is where u want to mount it, ext3 is the filesystem type, fuck knows what defaults is, 1 tells it to ensure the file system is updated prior to shutting down to ensure the data is on the disk (this is dumping)(persistance)(setting to 0 would be for netoworking file system, 1 for directly attached disks), and 2 is the order in which the check is happening as in when the machine is turned on and 2 is for disks attached to the machine





## Setting up load balancer with HA proxy on Ubuntu

- What is a load balancer?
  - It's basically a reverse proxy server with more configurations which distributes (balances) traffic between 2 or more servers
  - For traffic where the server capacity is overloaded the load balancer obviously balances the load
  - There are different algorithms which achieve this 
    - e.g. Round Robin
    - Least Connections
      - If one user asking for a lot of requests a round robin could overload but least connections would be on it ez
    - etc.
- The package manager of ubuntu is `apt` 
  - Use this for package manager install commands 
- on ubuntu u need to update source lists 
- we openened two ubuntu instances to connect normally through aws

- We opened another ubunut instance to act as our load balancer and we install HA Proxy On it
  - HA proxy is a software like proxy software?
    - used commands from its website to install it 
- Editing the ha proxy config file 
  - with information from the internet  "how to use haproxy to set up https load balancing on an ubuntu vps" 
  - Use internal (private) server ips as it's faster to connect and the connection is going to be secure between them maybe
  - you must then `sudo systemctl restart haproxy`
  - If error comes up use `haproxy -f haproxy.cfg` to see error messages
  - the `.cfg` wasnt complete because there wasnt a part which told the frontend servers where the backend server is, apparently that always must be told

---
- EXAMPLE FORMATTING FOR `cfg` files
```
#
# This is the ultimate HAProxy 2.0 "Getting Started" config
# It demonstrates many of the features available which are now available 
# While you may not need all of these things, this can serve
# as a reference for your own configurations.
#
# Have questions?  Check out our community Slack:
# https://slack.haproxy.org/
#

global
    # master-worker required for `program` section
    # enable here or start with -Ws
    master-worker
    mworker-max-reloads 3
    # enable core dumps
    set-dumpable
    user haproxy
    group haproxy
    log stdout local0
    stats socket 127.0.0.1:9999 level admin expose-fd listeners
    tune.ssl.default-dh-param 2048
    ssl-default-bind-ciphers ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS
    ssl-default-bind-options no-sslv3 no-tls-tickets
    ssl-default-server-ciphers ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS
    ssl-default-server-options no-sslv3 no-tls-tickets
    # New strict-limits
    strict-limits
    # New directive to support misconfigured servers
    h1-case-adjust cache-control CaChE-CoNtRoL
 
defaults
    mode http
    log global
    timeout client 5s
    timeout server 5s
    timeout connect 5s
    option redispatch
    option httplog

program dataplane-api
    command /usr/sbin/haproxy-dataplaneapi --host 0.0.0.0 --port 5555 --haproxy-bin /usr/sbin/haproxy --config-file /etc/haproxy/haproxy.cfg --reload-cmd "systemctl reload haproxy" --reload-delay 5 --userlist api

program spoa-mirror
    command /usr/sbin/spoa-mirror -r0 -u"http://192.168.1.7/"
    
program whoami
    # Prints username 'haproxy' to logs
    command /usr/bin/whoami
    # New user and group directives
    user haproxy
    group haproxy

peers mypeers
    bind :10001 ssl crt /etc/haproxy/certs/www.example.com.pem 
    default-server ssl verify none
    server PC #local peer.  name must match local server name
    table src_tracking type string size 10m store http_req_rate(10s),http_req_cnt 

resolvers dns
    parse-resolv-conf
    resolve_retries       3
    timeout resolve       1s
    timeout retry         1s
    hold other           30s
    hold refused         30s
    hold nx              30s
    hold timeout         30s
    hold valid           10s
    hold obsolete        30s

userlist api 
  user admin password $5$aVnIFECJ$2QYP64eTTXZ1grSjwwdoQxK/AP8kcOflEO1Q5fc.5aA

frontend stats
    bind *:8404
    # Enable Prometheus Exporter
    http-request use-service prometheus-exporter if { path /metrics }
    stats enable
    stats uri /stats
    stats refresh 10s

frontend fe_main
    bind :80 
    bind :443 tfo ssl crt /etc/haproxy/certs/www.example.com.pem alpn h2,http/1.1

    # Enable log sampling
    # One out of 10 requests would be logged to this source
    log 127.0.0.1:10001 sample 1:10 local0
    # For every 11 requests, log requests 2, 3, and 8-11
    log 127.0.0.1:10002 sample 2-3,8-11:11 local0

    # Log profiling data
    log-format "%ci:%cp [%tr] %ft %b/%s %TR/%Tw/%Tc/%Tr/%Ta %ST %B %CC %CS %tsc %ac/%fc/%bc/%sc/%rc %sq/%bq %hr %hs %{+Q}r cpu_calls:%[cpu_calls] cpu_ns_tot:%[cpu_ns_tot] cpu_ns_avg:%[cpu_ns_avg] lat_ns_tot:%[lat_ns_tot] lat_ns_avg:%[lat_ns_avg]"
    
    # gRPC path matching
    acl is_grpc_codename path /CodenameCreator/KeepGettingCodenames 
    # Dynamic 'do-resolve' trusted hosts
    acl dynamic_hosts req.hdr(Host) api.local admin.local haproxy.com

    # Activate Traffic Mirror
    filter spoe engine traffic-mirror config mirror.cfg

    # Redirect if not SSL
    http-request redirect scheme https unless { ssl_fc }

    # Enable src tracking
    http-request track-sc0 src table mypeers/src_tracking

    # Enable rate limiting
    # Return 429 Too Many Requests if client averages more than
    # 10 requests in 10 seconds.
    # (duration defined in stick table in peers section)
    http-request deny deny_status 429 if { sc_http_req_rate(0) gt 10  }

    # Enable local resolving of Host if within dynamic_hosts ACL
    # Allows connecting to dynamic IP address specified in Host header
    # Useful for DNS split view or split horizon
    http-request do-resolve(txn.dstip,dns) hdr(Host),lower if dynamic_hosts
    http-request capture var(txn.dstip) len 40 if dynamic_hosts

    # return 503 when dynamic_hosts matches but the variable 
    # txn.dstip is not set which mean DNS resolution error
    # otherwise route to be_dynamic
    use_backend be_503 if dynamic_hosts !{ var(txn.dstip) -m found }
    use_backend be_dynamic if dynamic_hosts

    # route to gRPC path
    use_backend be_grpc if is_grpc_codename 
    
    # Route PHP requests to FastCGI app
    use_backend phpapp if { path_end .php }

    default_backend be_main

backend be_main
    default-server ssl verify none alpn h2 check maxconn 50
    # Enable Power of Two Random Choices Algorithm
    balance random(2)
    # Enable Layer 7 retries
    retry-on all-retryable-errors
    retries 3 

    # retrying POST requests can be dangerous
    # make sure you understand the implications before removing
    http-request disable-l7-retry if METH_POST

    server server1 192.168.1.13:443 tfo
    server server2 192.168.1.14:443 tfo
    server server3 192.168.1.15:443 tfo
    server server4 192.168.1.16:443 tfo
    server server5 192.168.1.17:443 tfo
    
    # New fetch methods:
    http-response add-header SERVER_NAME "%[srv_name]"
    http-response add-header SERVER_QUEUE "%[srv_queue(server1)]"
    http-response add-header UUID "%[uuid]"

    # New 'sha2' converter:
    # set a value to hash, such as a username
    http-response set-var(res.username) str("JOE SMITH")
    # create hash with combined username and secret and convert to hexadecimal
    http-response set-var(res.checksum) var(res.username),concat("secret"),sha2(256),hex
    # Save full cookie header value
    http-response set-var(res.cookieval) str(),concat("Username=",res.username,"|"),concat("",res.checksum)
    # set cookie. Creates header 'set-cookie: Username=JOE SMITH|4DA485B06BB9D30DF34AC4BB1696BA9DA850DB074727E87C4B05448BD07218DF'
    http-response set-header Set-Cookie %[var(res.cookieval)]

backend be_grpc
    default-server ssl verify none alpn h2 check maxconn 50
    server grpc1 10.1.0.11:3000 
    server grpc2 10.1.0.12:3000 

backend be_dynamic
    default-server ssl verify none check maxconn 50

    # rule to prevent HAProxy from reconnecting to services
    # on the local network (forged DNS name used to scan the network)
    http-request deny if { var(txn.dstip) -m ip 127.0.0.0/8 10.0.0.0/8 }
    http-request set-dst var(txn.dstip)
    server dynamic 0.0.0.0:0

backend spoe-traffic-mirror
    mode tcp
    balance roundrobin
    timeout connect 5s
    timeout server 1m
    server spoa1 127.0.0.1:12345
    server spoa2 10.1.0.20:12345

backend be_503
    # dummy backend used to return 503.
    # You can use the 'errorfile' directive to send a nice
    # 503 error page to end users.
    errorfile 503 /etc/haproxy/errorfiles/503sorry.http

# New section for FastCGI
fcgi-app php-fpm
    log-stderr global
    option keep-conn
    docroot /var/www/html
    index index.php
    path-info ^(/.+\.php)(/.*)?$

backend phpapp
    # User FastCGI
    use-fcgi-app php-fpm
    server server1 127.0.0.1:9000 proto fcgi

backend wonky_server
    # Enable case-sensitive header option
    option h1-case-adjust-bogus-server
    server server2 127.0.0.1:8081 check
```
```
##based on Mesosphere Marathon's servicerouter.py haproxy config

global
  daemon
  log 127.0.0.1 local0
  log 127.0.0.1 local1 notice
  maxconn 4096
  tune.ssl.default-dh-param 2048

defaults
  log               global
  retries           3
  maxconn           2000
  timeout connect   5s
  timeout client    50s
  timeout server    50s

listen stats
  bind 127.0.0.1:9090
  balance
  mode http
  stats enable
  stats auth admin:admin

frontend microservice_http_in
  bind *:80
  mode http

frontend microservice_http_appid_in
  bind *:81
  mode http
  acl app__accountCreationService hdr(x-microservice-app-id) -i /accountCreationService
  acl app__profileEditingService hdr(x-microservice-app-id) -i /profileEditingService
  use_backend accountCreationService_10000 if app__accountCreationService
  use_backend profileEditingService_20000 if app__profileEditingService

frontend microservice_https_in
  bind *:443 ssl crt /etc/ssl/yourCertificate
  mode http

frontend accountCreationService_10000
  bind *:10000
  mode http
  use_backend accountCreationService_10000

frontend profileEditingService_20000
  bind *:20000
  mode http
  use_backend profileEditingService_20000

backend profileEditingService_20000
  balance roundrobin
  mode http
  option forwardfor
  http-request set-header X-Forwarded-Port %[dst_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
  server 151_256_250_152_35900 151.256.250.152:35900
  # additional servers here

backend accountCreationService_10000
  balance roundrobin
  mode http
  option forwardfor
  http-request set-header X-Forwarded-Port %[dst_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
  server 101_206_200_192_31900 101.206.200.192:31900
  # additional servers here
```
---

### Security of load balancing system
  - The two webservers are in it its own subnet and the load balancer is in its own one
    - However the two subnets should be able to communicate via a route table
  - Secuirty Groups protect each server 
- 


## Private and Public IP's
  - Private IP's are used to talk to things within a network 
    - This is easier as if you used a public ip it would have to exit the server and go back in which is long
    - Also cheaper as cost is based on traffic in and out
  - Public IP used to talk to things outside the network

## AMI (Amazon Machine Images) but more generally Images
- The image is a snapshot of the data of the setup of the server including the data on the servers harddrive
- Includes drivers and all that
- In part of CI/CD there are continuous tests to see whats good and that
  - When they have passed, a snapshot of a working system is created which is ready for deployment.
    - If these tests faill, notify the dev teams
  - Snapshots should be created when 100% everything is working, the tests are important for this
- When we launch an instance from these ami's (for example the jspaint one)
  - We want some commands to be executed already
  - like the `sudo systemctl start jspaint`
  - This can be added in the configure instance tab when setting up the machine on aws
- CI/CD(CD) Pipeline
  - These images are useful within this pipeline
  - Continuous integration
    - automatic testing and development
  - Continuous Delivery 
    - Delivers AMI, code and environment are ready)(however code is packaged and user hasn't seen
  - Continuous Deployment
    - Deployed code is live, client and users have access and can use it 

## Keeping costs down
- Stop your machines when you're not using them fam
  - Save that 4p an hour, it adds up 
- Spot instances
  - Bid to use computer power when no one else is using it
  - These instances could be used for when time is not tight deadline like a ml algorithms on data being run at night.
- Right size machines 
  - WIth experience, trial and error, system requirements it would be possible to figure out the appropriate size of machine used
  - Using a bigger machine when not needed is an obvious waste of cost.
- Bandith - Private IP & Data Access (optimizing SQL queries)
  - When sending requests for data from a db, optimizing(? specifically how?) is a way to keep costs down
  - Also optimizing external api requests
- Storing data
  - Different types of storages provide different monetary costs at the price of speed of retrieving data and such
    - S3 - simple storage from amazon always ready
    - S3 Glacier - even simpler and cheaper but takes longer to access data

## CLI Scripting and Bash
- AWS CLI
  - Amazon web services command line interface
  - A usual connection to aws through website requires api keys and api secret keys to connect
  - Allows to connect to aws through a command line
    - Allows API/prgramatic access to aws resources
    - and declaratively create and launch instances 
  - Its an abstraction of AWS
    - Removes a layer of complexity allowing an ease of interaction
  - Works through API end-point & conspution of AWS services via CLI 
  - The CLI/API of Amazon is the basis of many tools 
    - like Ansible and Terraform
- Main Commands
- `ln -s ~/Downloads/credentials ~/.aws/credentials` creates a symbolic link(I think thats what `ln -s` does, double check) between credentials in downloads and the one in `.aws`

## credentials vs ssh keys
- credentials lets aws identify you when you perform any operation on aws (like creating instances and other things)
  - Also restricts access to different projects and stuff based on security concerns
- ssh key is specific to logging on server with the ssh operation 

## Python (20 & 21 Apr)
- when dealing with floating point numbers, because of the way computers store them, there could be inaccuracies involved 
  - e.g. `10/3 = 3.3333333335` 
  
- to protect quote (called escaping)
  - `\` tells python the character following it has a special purpose
    - `s = 'this contains both quote type " \' '`
    - python reads the single quote right after the `\` and protects it to be part of the string and not a quote which ends the string
  - `s = 'this string contains backslash followed by single quote \' '`
    - This will however output: `"this string contains backslash followed by single quote ' "`
    - To protect the backslash itself to be within the quote, we need
      - `s = "this string contains backslash followed by single quote \\' "`
- `dir(<obj>)` will show a list of methods (functions) which can be called on the object 
  - e.g. `dir(l)` for a list `l` has a method `count` which counts the number of times an instance appears in the list
    - `l.count(3)` - count the number of times 3 appears in list `l`
- if we have a list within a list e.g. `l = [1,2,3,[a,b,c],6]`
  - `l[3]` would show `[a,b,c]`
  - and `l[3][1]` would show `b`
- a set is a list where only unique elements are listed and are unordered 
  - `w=set([1,2,3,4])'
    - the output would be `{1,2,3,4}`
  - or `w={4,4,4}` (another way of defining sets)
    - The output of this would be just `{4}`
- `list1.append(list2)` would add `list2` as an item into `list1`
  - while `list1.extend(list2)` would add the elments of `list2` as separate elements into `list1`

- TUPLES
  - You can't extend upon a tuple (wow thats deep), to add elements you need to redefine the tuple again with the new element
  - `t = (10,20,30)` defining a tuple
  - these are read only, fixed in memory lists so they're fast because no write: SPEED
- DICTIONARIES
  - key:value pairs, gives meaning to numbers
  - is an unordered list, what is the zeroth element???? `d[0]` would return an error
  - `d = { "a":1, "b":2, "c":3 }`
  - `d = { "fname":"wagwan", "sname":"son", "age":300 }`
  - to access an element(whats the right terminology)
    - `d["fname"]` would return `"wagwan"`
  - to add new item(element)
    - `d['city'] = 'London'`
    - the dictionary would then be: `d = { "fname":"wagwan", "sname":"son", "age":300, 'city': 'london'}`
  - `d.values()` would return list: `dict_values(["wagwan", "son", 300, "london"])`
  - `d.keys()` would return list:`dict_keys(["fname", "sname", "age", 'city'])`
  - `d.items` would return tuple pair of `(key,value)`
  - `d.update({'age':41})` would update the dictionary ADDING TO THE VALUES
    - e.g. `d = { "fname":"wagwan", "sname":"son", "age":300,41, 'city': 'london'}`


- use of `.format` and `fstring` for same output, for defined variables `a,b,c`
  - `print("{} times {} is {}".format(a, b, a * b ))`
  - `print(f"{a} times {b} is {a*b} also {c}")`
    - fstring not supported in python 2 or less, only avaliable in python 3

- use of `zip`
  - l1 = [1,3,5,7,9]
  - l2 = ['a','e','i','o','u']
  - `list(zip(l1,l2))` would list paired tuples
    - i.e. [(1,'a'),(3,'e'),(5,'i')...]
    - this is for lists of same lengths

  - l3 = [4,'hi']
  - `list(l1,l3)`
    - [(1,4),(3,'hi')] 
    - the zipped list is as the shortest individual list of the two

- `return None` - is a null object, and is it's own python type which returns "None" but not an empty string or anything like that ITS NOTHING

- when creating libraries, python helps make documentation when you use triple quotes just under the function:
    ```python
    def is_negative(n):
      """yo this function checks if n is negative
      """
      ...
    ```


- difference between using explicit functions and using lambda function to do the same thing
  - better to use lambda function if the explicit function is a short one liner, lambda function takes less space up and probably even runs quicker
  - example below
  ```python


  items = [1,3,5,7,9,11]

  print(f"Our starting list is {items}")

  ### different ways of printing the same thing


  ## first way
  def double_value(n):
    return n*2

  doubles_1 = list(map( double_value, items ))  ## the map function takes each item from "items" and maps it through the function "double_value" and stores it in a list, map() works like map( function_to_map_to(), list_from_where_values_are_taken )
  print(f"style 1:  the doubled values of each of the items are {doubles_1}")

  ## second way
  doubles_2 = list(map( lambda x: x*2, items )) ## the lambda function takes each item from "items" and multiplies it by two and stores it in a list
  print(f"style 2:  the doubled values are {doubles_2})
  ```

- unit testing
  - A script which tests lots of things automatically without us having to test all the things that could break our code

- api's, a standard way programs/computers can communicate information
  - json is a language format which shares data which apis use (that is some messed up wording)
  - Error codes:
    - Response 2XX: Success (200 means all good)
    - Response 3XX: EH kinda (for redirects of pages)
    - Response 4XX: you (the user) made a mistake  (404 not found)
    - Response 5XX: I (the server) made a mistake 

  - post request vs get request



## 22/04

- OOP vs Functional Programming
  - Functional Program:
    - It's a block of code
    - Takes arguments 
    - Are "anonymous" (? wtf does that mean)
    - Can be called from anywhere
    - Best Practices:
      - A function has one job
      - Allowing it to be testable (unit testing, function by function)
      - DRY code (Don't Repeat Yourself)
  - OOP (Object Oreinted Programing) Programming:
    - Programming by abstraction of 'real' objects or functionality of a system 
    - Has 4 pillars:
      - Abstraction
        - Ability to abstract (hide away) complexity/code (by creating classes/methods and storing the complexity within that and simply calling them?)
      - Inheritance 
        - Ability for child class to inherit behaviour (methods) from parent class
      - Polymorphism
        - Ability for different classes to be used with the same interface
        - The condition of occurring in several different forms
        - When two types share an inheritence chain, both can be used on objects interchangably without errors or (a change in result(? is this true))
      - Encapsulation
        - Restricting access to things
    - Method vs Function
      - A function is anonymouse
      - A method is a function within a class which required an object(instance) of that class to call it (by it i mean the method)

- Databases
  - A database is a collection of information which is organised for easy access and management
  - Relational DB - SQL
    - Structured Query Language
    - It has tables and relationships between tables
      - This relationship is managed with foreign and primary keys
      - Primary Key
        - It is either existing column or one specifically generated by db according to a defined sequence
        - Ensures data in specific column is unique
        - Uniquely identifies record of data in table
      - Foreign Key
        - This is a column or a group of columns that provides a link between data in two tables
        - Refers to a primary key in another table
        - Says that a record belongs to another record
    - First, Second, Third Normal Forms: 
      - Write some stuff about this

  - EXAMPLE DATABASE:
    - AuthorTable (first database)
      - ID - PrimaryKey
      - FirstName
      - LastName
      - Birthday
      - Email
      - Books Published

    - BooksTable (second database)
      - ID - PrimaryKey
      - Author_ID - ForeignKey (the ID from AuthorTable) (the note here is not to add a separate name but to link the ID straight from Author Database)
      - Title
      - Date_Release

    - This now creates a 1:M (One-To-Many relationship between AuthorTable and BooksTable as One Author can be connected to Many Books)
      - Lets just assume a book can only be written be one author no collab ting
      - Other possible relationships are (1:1, M:M)
  - There are different DB management system which serve different purposes, the best tools should be adpoted for the job
  - Examples of DB management systems:
    - MySQL
    - PostgreSQL
    - OracleDB

- creating db in postgres thru terminal
  - `sudo -u postgres createdb my_database`
- go into db
  - ` sudo -u postgres psql -d my_database`

  
- `CREATE TABLE songs (artist varchar(50), song varchar(50), position integer) WITH OIDS;`
  - creating a table 
    - creating table called songs
    - column artist with datatype character (which cannot be longer than 50 characters)
    - same with song name
    - position with integer datatype
    - `WITH OIDS` this is primary key?
      - WHAT THIS MEANS
    - `;` every command must end with this

- `\d` to view table 


- `INSERT INTO songs VALUES ('ABBA', 'Mamma Mia', 2);`
  - Inserting values

- `Select oid from songs;` 
  - Shows primary key in db ? 


- `\q` to quit database
- `COPY songs FROM '/home/ubuntu/favourite_songs.txt' ( DELIMITER(',') );` 
- format of this .txt file is important for example
  ```
  Frankie Goes to Hollywood,The Power of Love,1
  Huey Lewis and the News,The Power of Love,1
  Jennifer Rush,The Power of Love,1
  Bob Marley,Three Little Birds,3
  The Police,Every Breath You Take,2
  Frankie Goes to Hollywood,Relax,5
  Bob Marley,Jamming,3
  ```
  - copy `songs` (name of db) from the file which is comma separated
    - this comma separation is declared in the DELIMITED
  - running this multiple times just populates it and creates duplicate data

- `SELECT *, oid FROM songs;` 
  - Shows all songs in db `songs`
  - SELECT EVERYTHING AND THE OID FROM SONGS AND SHOW ME

- `SELECT artist, song AS song_title FROM songs where artist = 'Bob Marley';`
  - Shows only bob marley songs also changes `song` column to `song_title`


- ` SELECT DISTINCT song FROM songs ORDER BY song;` 
  - show unique entries if there are duplicate elemnts   

- `SELECT song FROM songs WHERE artist = 'The Police';`
  - Songs just by police
  - better to select to make sure what songs u gonna delete to verify


- `DELETE FROM songs WHERE artist = 'The Police';` 
  - better to run `SELECT song` above to make sure what songs u gonna delete to verify

- `DELETE FROM songs WHERE oid = '16559';` 
  - to delete specific rows by oid

- `SELECT songs.artist, musicians.age, songs.song FROM songs, musicians WHERE songs.artist=musicians.name;`
  - from songs table select artist, from musicians table select age, from songs table select song, ....
  - matching artists from song with name from musicians as they are same
  - this is an `inner join` because we're getting names which only appear in songs AND musicians tables

- `SELECT * FROM songs INNER JOIN musicians ON (songs.artist = musicians.name);`
  - This is an `outer join` so all the things are joined?? (wtf but it says INNER)

- `SELECT * FROM songs RIGHT OUTER JOIN musicians ON (songs.artist = musicians.name);`
  - wot

- `SELECT name FROM musicians WHERE age=max(SELECT max(age) FROM musicians);`
  - select within a select for more sophisticated queries 


- `CREATE TABLE music (id int, album varchar(20), price numeric CHECK (price >0));` 
  - last checks if positive number

- `INSERT INTO music VALUES (1, 'Gold', 50), (2, 'Silver', 15), (5, 'Bronze', 10), (10, 'Wood', 2);`


- `ALTER TABLE music RENAME COLUMN album TO albums;`

- `ALTER TABLE music ADD CHECK (price > 1);`

- `ALTER TABLE music DROP CONSTRAIGHT music_price_check` 
  - each constraint has its own name in this case "music_price_check"

- `ALTER TABLE music ADD COLUMN reduced_price numeric`

- `ALTER TABLE music ADD CHECK (price > reduced_price);` 
  - checks if the value in price column is greater than reduced_price

- `UPDATE music SET reduced_price=3 WHERE id=2`

- `UPDATE music SET price=price+5 WHERE albums='Silver';`



- `CREATE TABLE logins (id integer UNIQUE, username varchar(20), passcode varchar(20), UNIQUE (username, passcode));`

  - create dem uNIQUE entriyes fam


- `INSERT INTO logins VALUES (2, 'bob', 'passcode2')`
- `INSERT INTO logins VALUES (1, 'alice', 'passcode1')`


- `CREATE TABLE composers (era varchar(20)) INHERITS (musicians);`
  - this creates new table called composers, AND INHERITS COLUMNS FROM TABLE MUSICIANS
  - including the values within those columns



- `ROLLBACK` goes back to before "transaction?"
  - which is defined as all commands between `BEGIN` and `COMMIT`


- `ACID` COMPLIANCE (DO SOME REASERACH BECAUES WHAT IS THE PURPOSE)
- `Atomicity` 
  - you dont complete a transaction (e.g. updating 3 tables) until the transaction is completely done (e.g. all 3 tables are updated)
- `Consistency`
- `Isolation` 
  - Other peoples transactions shouldn't 
- `Durability`
  - Each transaction should run the same every time

- `EXPLAIN` before a command explains the command
  - for example `EXAMPLE SELECT * FROM songs ...`
  - shows that there is a `sequential scan` being performed on the list and shows the cost of running function 
    - a `sequential scan` is one after the other kinda scan

- `CREATE INDEX song_index ON songs (artist);` 
  - once youve added an index and run the `EXAMPLE SELECT * FROM songs ...` command 
  - the command fetches your output from the index not the table directly, reducing the cost which the `EXPLAIN` command shows
  - RULE OF THUMB: if `SELECT` command returns MORE THAN ~5-10% of the table
    - then sequential scan is FASTER than index scan

- `\x` is extended display
  - run command again to turn off
- `\t` is tuple display
- `\g` runs previous command again
- `\s` for history 
- `\h` help 


- `sudo -u postgres pg_dump -F t my_database >backup.tar`
  - dumps the db into tar file

- `sudo -u postgres pg_dump my_database >backup.sql`
  - dumps into sql with command history 

- `sudo -u postgres psql -d back_up -f backup.sql`
  - to bring back db from sql


- `sudo -u postgres pg_restore -dback_up_2 -v backup.tar`
  - to bring back db from tar as dback_up_2



- Commands used in `MySQL Workbench`
  ```sql
  USE msFilm;

  -- SELECT * FROM <table> (THIS IS A COMMENT BTW STARTED WITH: -
  SELECT * FROM Director;

  SELECT * FROM Film;


  -- SELECT ALL FROM Film WHERE FilmID = 106

  SELECT * FROM Film
  WHERE FilmID = 106;

  -- Select all data from films made after 1970

  SELECT * FROM Film
  WHERE DateMade > 1970;


  -- Select all data from films made before 1960

  SELECT * FROM Film
  WHERE DateMade < 1960;


  -- Select all data from films whose title starts with 'the'
    -- LIKE allows us to search and use wildcards for substitution
      -- LIKE means it's NOT case sensitive search
    -- % mean starts with this character and everything after can be anything
    -- "The%" means start with the and anything after can be anything
      -- "%Blue" means have anything before but must end with "blue"
      -- "%of%" means anything before and after but of must be in the middle

  SELECT * FROM Film
  WHERE Title LIKE "The%";

  SELECT * FROM Film
  WHERE Title LIKE "%blue";

  SELECT * FROM Film
  WHERE Title LIKE "%of%";


  -- If we know how many characters we want but not the character we can use "_"
    -- for example: any first 2 characters, third must be an 'a' and any number of characters after
      
  SELECT * FROM Film
  WHERE Title LIKE "__a%";


  -- For case sensitive requests use LIKE BINARY
  SELECT * FROM Film
  WHERE Title LIKE BINARY "%C%";
  -- to see the difference
  SELECT * FROM Film
  WHERE Title LIKE BINARY "%c%";



  -- Let's say we want all the films but only the title and date it was made
  -- SELECT column1, column2 FROM table

  SELECT Title, DateMade FROM Film;



  -- EXERCISES

  -- For films made AFTER 1959, show me: title, date, category
  SELECT Title, DateMade, CatID FROM Film 
  WHERE DateMade > 1959;


  -- For films made BETWEEN 1959 and 1970, show me: title
  SELECT Title, DateMade, CatID FROM Film
  WHERE DateMade > 1959 AND DateMade < 1970;

  -- OR, ONE BELOW IS BETTER

  SELECT Title, DateMade, CatID
  FROM Film
  WHERE DateMade BETWEEN 1959 AND 1970;

  -- For films made AFTER 1952 OR that contain 't' anywhere in the title, show me all data
  SELECT * FROM Film
  WHERE DateMade > 1952 OR Title LIKE "%t%";

  -- For films on all films except those made in 1944
  SELECT * FROM Film
  WHERE DateMade != 1944;

  -- Joining columns from two tables?

  SELECT Film.Title, Film.DateMade, Category.Descr
  FROM Film
  JOIN Category
  ON Film.CatID=Category.CatID;

  -- Notes on Joins
    -- It needs to make sense where the join is made, usually join primary keys with foreign keys 
      -- A Foreign key is a key which is primary elsewhere. (Primary keys is just the UNIQUE IDENTIFER)
    -- A way to think of this join is row by row, so for a row we look at Film.CatID and then look at that value in Category.CatID and JOIN that ROW onto the row we want


  SELECT Film.Title, Film.DateMade, Category.Descr
  FROM Film
  LEFT JOIN Category
  ON Film.CatID=Category.CatID;

  -- Its LEFT because to the LEFT of the command we specify FROM Film, the one below shows the same results

  SELECT Film.Title, Film.DateMade, Category.Descr
  FROM Category
  RIGHT JOIN Film
  ON Film.CatID=Category.CatID;


  -- FULL JOIN used probably to performance test DB

  SELECT * FROM Category
  FULL JOIN Film;


  -- Different kinds of JOIN
  -- Inner join (aka Join)
  -- Left or Right Join
  -- Outer Join

  
  -- Inserting entries with specific values (not adding a value for all features)
  INSERT INTO Film
  (Title, DateMade, DirID, Cost, FilmID)
  VALUES
  ('UP', 2012, 1, 3, 120)
  -- This doesn't need to be in the order in which columns are displayed
  -- The consistency just needs to be in the code 





  -- Aliasing

  SELECT Title, Cost, DateMade as 'Date Made'
  FROM File
  WHERE DateMade > 1970;

  
  
  
  -- When creating the tables, u must set column names and then assign them as primary keys and foreign keys and where they're from 
  
  CREATE TABLE `Creatures`.`Bookings` (
  `BookingID` int NOT NULL,
  `Date` VARCHAR(20),
  `UserID` int,
  `PetID` int,
  PRIMARY KEY (BookingID),
  FOREIGN KEY (UserID) REFERENCES Users(UserID),
  FOREIGN KEY (PetID) REFERENCES Creature_table(PetID) 
  );


  ```





## DATABASE TASK 23/04

- USER STORY
  - CORE STORIES
    
    - Need to add exotic/mythical creature to be a companian to pet it or go on an adventure
    
      - FEATURES, need to specify for table `creatures`: 
        - ( `creature_id`, `creature_name`, `creature_type_latin_name`, `creature_type`, `creature_abilities`, `creature_temper`, `creature_age`, `creature_emergency_contact` )
      - Create the database initially and populate with random names 
    
    - Need to be able to book this creature for an adventure
      

      - Need a `booking_info` table and a `users_info` table
        - `booking_info` table must have features: 
          - ( `booking_id`, `creature_id`, `user_id`, `booking_length_of_time`, `booking_rate`)

        - `users_info` table must have features:
          - (`user_id`, `booking_id`, `creature_id`, `user_first_name`, `user_last_name`, `user_contact_number`, `user_email`)






  - SIDE STORIES
    - Want to be able to `search and browse for all creatures`
      - 
    - To be be able to `search creatures based just on creature type`
      - Use name generator to populate with names 
    - To be able to `search based on creature name`




1,22,Ramana,Patel,ramana.patel@gmail.com,07285637281
2,12,John,Adams,lilpandie25@gmail.com,07334518273
3,34,Peter,Sanders,peterthebest@gmail.com,07846283716
4,14,Selina,Richardson,selina123@gmail.com,07736453647
5,25,Mary,Seacole,marythegod@gmail.com,07947462513


bookid,creatureid,userid,bookinglengthtime,bookrate

22,2,1,10,2
12,31,2,20,5
34,21,3,120,1
14,19,4,65,10
25,45,5,100,23

- `ALTER TABLE <table_name> ADD PRIMARY KEY id;` to add primary key to column name `id`




## 26/04 - Connecting to our database through python

- We opened new python project and installed `mysql-connector`
  - Which we imported through `import mysql.connector`
- `import sys`
  - This is a standard python library and gives system specific methods, gives information on system and can ge files and dat

```python
dbcur = cnx.cursor(dictionary=True)
```
  - These tables are formatted as python dictionaries

```python
try:
    cnx = mysql.connector.connect(
        user = user_use,
        password = password_use,
        host = host_db,
        database = database_use
    )
except:
    print("Could not connect to DB. Please check your code & connection")
    sys.exit(1)
```
  - We put the connection statement into a try/except to catch any errors with a neat error message

- For these databases we want create code that allows us to `CRUD` each database as a main/min functionality
  - `Create`
  - `Read`
  - `Update`
  - `Destroy`
  - Not every database should have/need all of this functionality, depends on context 
  - For example for a DB with tables `pubs`, `msfilm`, `creatures` we would want to CRUD some features from these but not all of them
  
- To do this we're going to create classes, we create a main class which includes the conncetion to the DB 
  - Then we create child classes which has methods for each table within the DB  



## Jenkins 26/04

- Automation server which helps automate parts of software development like building, testing and deploying
- An automation server sits in the pipeline waiting for triggers to perform actions 



- we created a new project and started a bitbucket to which we connected
- `ssh-keygen -t ed25519 -C "ramana@automationlogic.com"`
  - we first created a new ssh key pair so that we can give it to jekins so it can access our bitbucket


- notes on the task of scp/git cloning files into a server, the files is a .sh webserver provisioning script
  - initally we had to allow jenkins to ssh into our server
    - we first created a new ssh key pair
    - to do this we went into ssh folder in the server and added our new public key into the file 'authorized_keys'
  - then for the git cloning i created 3 separate jobs
      - im unsure as to how separate jobs out so 4 was arbirtary as far as im concerned rn 
    - these 3 jobs were scheduled to execute one after the other 
    - the first job ssh into server and installed git into the server
    - the second job git cloned my repo into the file 
    - the third job then ran the .sh provisioning script which created the website. 
  - for the scp task, i did a similar thing
    - created 2 jobs where the first one scp just the .sh file from the jenkins workspace into server
      - the jenkins workspace was connected directly to the git repo and pulled the files from there
    - the second job just executes it 
  - the plan was for the first jobs to execute as soon as a new push was made to the git repo (using a webhook link?) but this flopped for some reason.


- worker vs main machine (part of best practices)
  - one of the main problems devops tries to solve is the destandardisation of enviroments
    - it solves this by standaradised environments, wow
  - a worker node is a standardised environment with a lot of tasks