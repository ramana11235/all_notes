# Setting up AWS Machine ec2 with jspaint

## Introduction
In this project we will make a bash script to provision a machine in aws to run js paint. This was an undertaking and a half. But a rewarding one nonetheless: we came out stronger and wiser ready to take on the rest of the DevOps world. 

When creating scripts we learnt that it is of the utmost imperative and common sense to get everything working manually and then write a script which executes every step automatically. So the first step was to provision the server ourselves. 

This required reading the jspaint README documentation which was incredibly necessary. It would have been a fruitless endeavour with nothing but tears if the README had not existed. We probably would have had to try a different app. Anyway, following the instructions was straightforward in the end. There was an initial confusion as we we were trying to recreate jspaint as desktop app which took up most of the day, but we then saw the instructions to create a live server version right underneath what we were reading the whole time. That was fun. 

The script we wrote used `scp` as the first command onto the server. I think copying in the file elongated the process as we had to use `sed` to change a line in the `.init` file. But I think if we used it near the end we wouldn't have to use `sed` and the `.init` file could have just had the replaced line initially. But we gained the experience of `scp` and `sed`, and what is life if not the gaining of experience. 

We also created a reverse proxy within the server which rerouted any ip which connecting via `Port 80` to `Port 8080`. `I still don't get what ProxyPass and ProxyPassReverse is GOOGLE IT` All these network related settings were done in the Apache webserver (`httpd`) config file.

As jspaint is a javascript app, we needed to install `nodejs` which is a runtime environment on which jspaint can run. The dependencies of jspaint was installed via the node package manager (`npm`). 

We also launched the app via a `.init` file we created. 

---

## Subjects Covered
This project will help us cover

1. Git
2. Bitbucket
3. Markdown
4. AWS 
   1. ec2 Creation
   2. Security Groups
5. Redhat
   1. Installation Process
   2. Scripts

6. Bash 
   1. ssh 
   2. scp 
   3. heredoc
   4. Outputs (stdin, stdout, stderr)
   5. Piping and grep

---

## Final Script

```bash

#!/bin/bash
if (( $# > 0 )) 
then
  hostname=$1 ## $ stands for input arguments in command line 
else
  echo "WTF: you must supply a hostname or IP address" 1>&2  ## (1>&2): map stdout to stderr
  exit 1 ## exit 1 (anything that not 0) is recognized a fail exit 
  ## this whole else is a fail message 
fi



## (-o) option allows for the following option StrictHostKeyChecking (-o allows for a lot more options not just StrictHostKeyChecking)
## StrictHostChecking is an option which doesnt add a fingerprint (when set to no) (less strict checking)
## The single quote allows for multiple commands to be run, needs to be right after the log in (on the same line). if its on the line after, itll: log in to the server, press enter, and wait for input 

## general outline of this script 
### get the code
### get the relevant code compiler (npm)
### install dependancies
### start the server (init script)
### Setup nodejs
### Setup reverse proxy

scp -o StrictHostKeyChecking=no -i ~/.ssh/RamanaArulljothiechanthiranKey.pem jspaint.init ec2-user@$hostname:jspaint.init 

ssh -o StrictHostKeyChecking=no -i ~/.ssh/RamanaArulljothiechanthiranKey.pem ec2-user@$hostname ' 

sudo yum -y install httpd
sudo yum -y install git

git clone https://github.com/1j01/jspaint.git

cd ~/jspaint


VERSION=v14.16.1
DISTRO=linux-x64

wget https://nodejs.org/dist/$VERSION/node-$VERSION-$DISTRO.tar.xz


sudo mkdir -p /usr/local/lib/nodejs
sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs

echo "# Nodejs 
VERSION=v14.16.1
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH" >>~/.bash_profile

source ~/.bash_profile

npm i

node -v



echo "installation and tests done"

sudo systemctl start httpd

cd /etc/httpd/conf.d

sudo sh -c "echo \"<VirtualHost *:80>
  ProxyPreserveHost On

  ProxyPass / http://127.0.0.1:8080/
  ProxyPassReverse / http://127.0.0.1:8080/

</VirtualHost>\" >jspaint.conf"

sudo systemctl restart httpd


cd ~/jspaint


sudo mv ~/jspaint.init /etc/init.d/jspaint


sudo chmod +x /etc/init.d/jspaint

sudo sed -i "s,PATH=\$PATH,PATH=\$PATH:/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/," /etc/init.d/jspaint

sudo chkconfig --add jspaint
sudo systemctl enable jspaint
sudo /etc/init.d/jspaint start

'


```
---

## Final .init File

```bash
#!/bin/bash

#description: JSPaint web app
#chkconfig: 2345 99 99

PATH=$PATH

case $1 in
	'start')
		cd /home/ec2-user/jspaint
		nohup npm run dev &
		;;
	'stop')
		kill -9 $(ps -ef | egrep 'npm|node' | awk '{print $2}')
		;;
esac
```

---

## A (Near) Line By Line Explanation 

```bash
scp -o StrictHostKeyChecking=no -i ~/.ssh/RamanaArulljothiechanthiranKey.pem jspaint.init ec2-user@$hostname:jspaint.init 
```
   - `scp` performs a secure copy of a file from the local machine to the server
   - `StrictHostKeyChecking=no` option means the fingerprint won't be saved and we won't have to manually select yes for that option
 - `jspaint.init ec2-user@$hostname:jspaint.init` the first `jspaint.init` specifies the file being copied from the local machine. The second `jspaint.init` after the `:` is the path it will be copied into and the name the file will have, in the case the file gets copied into the home directory keeping the same name.
   - `jspaint.init ec2-user@$hostname:/file1/hello/oogabooga`
   - In this case `jspaint.init` from the local machine get's copied into the directory `/file1/hello` and gets renamed to `oogabooga`

```bash
cd ~/jspaint


VERSION=v14.16.1
DISTRO=linux-x64

wget https://nodejs.org/dist/$VERSION/node-$VERSION-$DISTRO.tar.xz
```
   - This is the process to install `nodejs`
   - We change directory into ~/jspaint
   - And set variable names `VERSION` and `DISTRO`
   - `wget` downloads the file from the link into our current directory (`~/jspaint`)

```bash
sudo mkdir -p /usr/local/lib/nodejs
sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs
```
   - We then make then make a branch ending at the file `nodejs` 
   - In which we unzip the file we downloaded
     - `-C` option allows us to specify the directory
   - This (I THINK) is installing 
  
```bash
echo "# Nodejs 
VERSION=v14.16.1
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH" >>~/.bash_profile

source ~/.bash_profile
```
   - We then update the environment variables and the `PATH` variable with the path to `nodejs` we've just unzipped.

```bash
npm i

node -v
``` 
   - This installs `nodejs` from `npm`? bro im confused
   - And checks the version of `nodejs` we've installed