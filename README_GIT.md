# GIT & BitBucket Documentation

## Intro 
Git is a software which manages and keeps track of version control. It's possible to revert back to specific versions if necessary, for example if some dude messes up big time, its ok because git history has your back.

This is useful for collaboration as multiple users can make changes, keep track of them, and finally merge them into a master branch when necessary.

It does the managing and tracking by creating time lines. 

---

# Git Commands

### Creating a Repo
- `git init`
- This initalises a 'Master Branch'


### Checking Status of Files in Repo
- `git status`

### Adding Files To Commit To History
- `git add . `
- This adds all the files in the entire directory

### For separate files (e.g. file1) getting ready to commit
- `git add file1`

### To actually commit the files to timeline, with -m for informative message
- `git commit -m "insert helpful message about changes here"`

### To go to previous commit with hash 599f...
- `git checkout 599f...`
- You can find this hash from command `git log`


### To Connect To A Repo
- First create a Repo on BitBucket or Github
- Then from obtained `git` and `ssh` info
- `git remote add origin <git:ssh>`
  - use command to connect local git history to repo

-   And then `git remote --v` to see if it knows where origin is

### To Push To Repo

- `git push origin master`
- This pushes your `master` branch to the `origin` server


--- 

