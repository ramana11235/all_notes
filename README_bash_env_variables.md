# Environment Variables in BASH

This document describes an environment, the variables within it and commands associated with it.

---
- An environment is a place where code runs 
  - This code be local machine, dev machine, testing machine, production machine, etc. 
  - While you can view code in Github and Bitbucket, they are NOT environments as code cannot run there
- An environment variable is a variable that exists unique to the environment
  - These need to be set explicitly with things like Environment variable injectors and vaults
  - They are useful for the terminal, machine, or user
  - OR they are sensitive information like API keys which identifies a user/system, etc.
  - They are also used to set up IP of a DB (databse) or other services
- What is a PATH?
  - these are the files that the shell/terminal reads before opening 
  - The PATH usually sets: 
    - What programs does it know?
      - Such as brew, vagrant, jq, etc.
    - What alias do I know?
      - Alias' acts as a shortcut to programs 
    - What variables do I know?
    - It also sets formatting/themes and such
- On the terminal 
  - `env` to check environement vairable

--- 

- In this lesson we looked variables, environments, child/parent processes

- The first exercise was setting a variable in memory
  - We defined a variable in the command line `NAME=ADAM`
    - We called that variable with `echo $NAME`
  - Once logging out and logging back in the variable was no longer defined. A temporary existance in an impermanent place. 

- We then defined a variable in `.bash_profile`

- Then made a file to store variables and ran it as a child proess
  - `#!/bin/bash` the "hash bang" tells the interpreter to run from bash



- difference between `.bash_profile` and `.bashrc` is that bash profile is always loaded up, bashrc is extra information that can be loaded when needed
  - you "enter `.bashrc` state"(?) by `source .bashrc`

- `.bash_profile` is only read once to set up the environment and the variables within them (environment variables)
  - `.bashrc` is executed everytime an interactive shell is executed(?)

- `source .bashrc` updates the environment with new .bashrc file 

- We morphed these variables using export within that file 

--- 
## Notes - To - Sort 

- Add an Intro
- Make it more readable by splitting into sections